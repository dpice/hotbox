local sys  = require "luci.sys"
local uci  = require "luci.model.uci".cursor()

local runningtxt = translate("<b><font color=green>Breakme is running.</font></b>")
local notrunningtxt = translate("<b><font color=red>Breakme is not running.</font></b>")
local m, s

local running=(sys.call("pidof ss-redir > /dev/null") == 0)
m = Map("breakme", translate("breakme config"), running and runningtxt or notrunningtxt)

s = m:section(TypedSection, "breakme", "")
s.addremove = false
s.anonymous = true

s:tab("basic", translate("Basic Setting"))

enable = s:taboption("basic",Flag, "enabled", translate("Enable"))
enable.rmempty = false
function enable.cfgvalue(self, section)
	return sys.init.enabled("breakme") and self.enabled or self.disabled
end
function enable.write(self, section, value)
        if value == "1" then
                sys.call("/etc/init.d/breakme enable >/dev/null")
                sys.call("/etc/init.d/breakme start >/dev/null")
        else
                sys.call("/etc/init.d/breakme stop >/dev/null")
                sys.call("/etc/init.d/breakme disable >/dev/null")
        end
        Flag.write(self, section, value)
end

server_addr = s:taboption("basic", Value, "server_addr", translate("Server Address"))
server_addr.rmempty = false

server_port = s:taboption("basic", Value, "server_port", translate("Server Port"))
server_port.datatype = "port"
server_port.rmempty = false

server_pass = s:taboption("basic", Value, "server_pass", translate("Server Password"))
server_pass.rmempty = false

method = s:taboption("basic", ListValue, "method", translate("Server Method"))
method.rmempty = false
method:value("aes-256-cfb", "aes-256-cfb")
method:value("aes-128-ctr", "aes-128-ctr")
method.default = "aes-256-cfb"

local_port = s:taboption("basic", Value, "local_port", translate("Local Socks Port"))
local_port.datatype = "port"
local_port.placeholder = "1080"
local_port.rmempty = false

s:tab("exdomaintab", translate("Extra Domains"))

exdomain = s:taboption("exdomaintab", DynamicList, "exdomain", translate("Extra Domains"), translate("one domain per line."), "")

s:tab("exiptab", translate("Extra Ips"))

exip = s:taboption("exiptab", DynamicList, "exip", translate("Extra Ips"), translate("one ip/block per line."), "")

return m
