module("luci.controller.breakme", package.seeall)

function index()
	if not nixio.fs.access("/etc/config/breakme") then
		return
	end
	local page
	page = entry({"admin", "services", "breakme"}, cbi("breakme"), _("Break Wall"), 200)
	page.i18n = "breakme"
	page.dependent = true
end
