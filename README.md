## HotBox

Custom firmware for my x86/64 router base on openwrt, with some useful addons found from community.

### customize detail

- openwrt: Image size 800M, include boot partition size 100M.
- addon: luci-app-xlnetacc From https://github.com/sensec/luci-app-xlnetacc
- addon: vlmcsd & luci-app-vlmcsd From https://github.com/coolsnowwolf/lede
- addon: vsftpd-alt & luci-app-vsftpd
